package server;

import com.google.common.base.Charsets;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.servlets.assets.AssetServlet;
import io.voosh.shared.Logging;

import java.util.logging.Logger;

public class StaticAssetsBundle extends AssetsBundle
{
    private final Logger log = Logging.get(this);

    public StaticAssetsBundle(String path)
    {
        super(path);
    }

    public StaticAssetsBundle(String resourcePath, String uriPath)
    {
        super(resourcePath, uriPath);
    }

    public StaticAssetsBundle(String resourcePath, String uriPath, String indexFile)
    {
        super(resourcePath, uriPath, indexFile);
    }

    public StaticAssetsBundle(String resourcePath,String uriPath, String indexFile, String assetsName)
    {
        super(resourcePath, uriPath, indexFile, assetsName);
    }

    @Override
    protected AssetServlet createServlet()
    {
        return new StaticAssetsServlet(getResourcePath(), getUriPath(), getIndexFile(), Charsets.UTF_8);
    }
}
