package server;

import com.google.common.base.CharMatcher;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.collect.ImmutableList;
import com.google.common.hash.Hashing;
import com.google.common.io.Resources;
import com.google.common.net.HttpHeaders;
import com.google.common.net.MediaType;
import io.dropwizard.servlets.assets.AssetServlet;
import io.dropwizard.servlets.assets.ByteRange;
import io.dropwizard.servlets.assets.ResourceURL;
import io.voosh.shared.Logging;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;
import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

import static com.google.common.base.Preconditions.checkArgument;
import static java.util.Optional.empty;
import static java.util.Optional.of;


public class StaticAssetsServlet extends AssetServlet
{
    public static final int YEAR_IN_MINUTES = 365 * 24 * 60 * 60;

    private static final CharMatcher SLASHES = CharMatcher.is('/');
    private final Logger log = Logging.get(this);

    private static class CachedAsset {
        private final boolean isGzipped;
        private final byte[] resource;
        private final String eTag;
        private final long lastModifiedTime;

        private CachedAsset(byte[] resource, long lastModifiedTime, boolean isGzipped)
        {
            this.isGzipped = isGzipped;
            this.resource = resource;
            this.eTag = '"' + Hashing.murmur3_128().hashBytes(resource).toString() + '"';
            this.lastModifiedTime = lastModifiedTime;
        }

        public byte[] getResource() {
            return resource;
        }

        public String getETag() {
            return eTag;
        }

        public long getLastModifiedTime() {
            return lastModifiedTime;
        }

        public boolean isGzipped()
        {
            return isGzipped;
        }
    }

    private static final MediaType DEFAULT_MEDIA_TYPE = MediaType.HTML_UTF_8;

    private final String resourcePath;
    private final String uriPath;
    private final String indexFile;
    private final Charset defaultCharset;

    public StaticAssetsServlet(String resourcePath, String uriPath, String indexFile, Charset defaultCharset)
    {
        super(resourcePath, uriPath, indexFile, defaultCharset);
        final String trimmedPath = SLASHES.trimFrom(resourcePath);
        this.resourcePath = trimmedPath.isEmpty() ? trimmedPath : trimmedPath + '/';
        final String trimmedUri = SLASHES.trimTrailingFrom(uriPath);
        this.uriPath = trimmedUri.isEmpty() ? "/" : trimmedUri;
        this.indexFile = indexFile;
        this.defaultCharset = defaultCharset;
    }

    public URL getResourceURL() {
        return Resources.getResource(resourcePath);
    }

    public String getUriPath() {
        return uriPath;
    }

    public String getIndexFile() {
        return indexFile;
    }

    @Override
    protected void doGet(HttpServletRequest req,
                         HttpServletResponse resp) throws ServletException, IOException {
        try {

            final StringBuilder builder = new StringBuilder(req.getServletPath());
            if (req.getPathInfo() != null) {
                builder.append(req.getPathInfo());
            }
            final CachedAsset cachedAsset = loadAsset(builder.toString());

            if (cachedAsset == null)
            {
                resp.sendError(HttpServletResponse.SC_NOT_FOUND);
                return;
            }



            if (isCachedClientSide(req, cachedAsset)) {
                resp.sendError(HttpServletResponse.SC_NOT_MODIFIED);
                return;
            }

            //Custom: Set gzipped + cache + nocache headers
            if (cachedAsset.isGzipped())
            {
                resp.setHeader(HttpHeaders.CONTENT_ENCODING, "gzip");
                resp.setHeader("X-gzipped", "true"); //Used in tests
            }

            if (req.getPathInfo().contains(".nocache"))
                setNoCacheHeaders(resp);
            else if (req.getPathInfo().contains(".cache"))
                setCacheHeaders(resp);

            final String rangeHeader = req.getHeader(HttpHeaders.RANGE);

            final int resourceLength = cachedAsset.getResource().length;
            ImmutableList<ByteRange> ranges = ImmutableList.of();

            boolean usingRanges = false;
            // Support for HTTP Byte Ranges
            // http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html
            if (rangeHeader != null) {

                final String ifRange = req.getHeader(HttpHeaders.IF_RANGE);

                if (ifRange == null || cachedAsset.getETag().equals(ifRange)) {

                    try {
                        ranges = parseRangeHeader(rangeHeader, resourceLength);
                    } catch (NumberFormatException e) {
                        resp.sendError(HttpServletResponse.SC_REQUESTED_RANGE_NOT_SATISFIABLE);
                        return;
                    }

                    if (ranges.isEmpty()) {
                        resp.sendError(HttpServletResponse.SC_REQUESTED_RANGE_NOT_SATISFIABLE);
                        return;
                    }

                    resp.setStatus(HttpServletResponse.SC_PARTIAL_CONTENT);
                    usingRanges = true;

                    resp.addHeader(HttpHeaders.CONTENT_RANGE, "bytes "
                                                              + Joiner.on(",").join(ranges) + "/" + resourceLength);
                }
            }

            resp.setDateHeader(HttpHeaders.LAST_MODIFIED, cachedAsset.getLastModifiedTime());
            resp.setHeader(HttpHeaders.ETAG, cachedAsset.getETag());

            final String mimeTypeOfExtension = req.getServletContext()
                                                  .getMimeType(req.getRequestURI());
            MediaType mediaType = DEFAULT_MEDIA_TYPE;

            if (mimeTypeOfExtension != null) {
                try {
                    mediaType = MediaType.parse(mimeTypeOfExtension);
                    if (defaultCharset != null && mediaType.is(MediaType.ANY_TEXT_TYPE)) {
                        mediaType = mediaType.withCharset(defaultCharset);
                    }
                } catch (IllegalArgumentException ignore) {
                    // ignore
                }
            }

            if (mediaType.is(MediaType.ANY_VIDEO_TYPE)
                || mediaType.is(MediaType.ANY_AUDIO_TYPE) || usingRanges) {
                resp.addHeader(HttpHeaders.ACCEPT_RANGES, "bytes");
            }

            resp.setContentType(mediaType.type() + '/' + mediaType.subtype());

            if (mediaType.charset().isPresent()) {
                resp.setCharacterEncoding(mediaType.charset().get().toString());
            }

            try (ServletOutputStream output = resp.getOutputStream()) {
                if (usingRanges) {
                    for (ByteRange range : ranges) {
                        output.write(cachedAsset.getResource(), range.getStart(),
                                            range.getEnd() - range.getStart() + 1);
                    }
                } else {
                    output.write(cachedAsset.getResource());
                }
            }
        } catch (RuntimeException | URISyntaxException ignored) {
            resp.sendError(HttpServletResponse.SC_NOT_FOUND);
        }
    }

    private CachedAsset loadAsset(String key) throws URISyntaxException, IOException {
        checkArgument(key.startsWith(uriPath));
        final String requestedResourcePath = SLASHES.trimFrom(key.substring(uriPath.length()));
        final String absoluteRequestedResourcePath = SLASHES.trimFrom(this.resourcePath + requestedResourcePath);

        URL requestedResourceURL = getResourceUrl(absoluteRequestedResourcePath);
        boolean isGzipped = false;

        if (ResourceURL.isDirectory(requestedResourceURL)) {
            if (indexFile != null) {
                requestedResourceURL = getResourceUrl(absoluteRequestedResourcePath + '/' + indexFile);
            } else {
                // directory requested but no index file defined
                return null;
            }
        }
        else
        {
            Optional<URL> gzippedUrl = getGzippedUrlIfExists(absoluteRequestedResourcePath);
            if ( gzippedUrl.isPresent())
            {
                requestedResourceURL = gzippedUrl.get();
                isGzipped = true;
            }

        }

        long lastModified = ResourceURL.getLastModified(requestedResourceURL);
        if (lastModified < 1) {
            // Something went wrong trying to get the last modified time: just use the current time
            lastModified = System.currentTimeMillis();
        }

        // zero out the millis since the date we get back from If-Modified-Since will not have them
        lastModified = (lastModified / 1000) * 1000;
        return new CachedAsset(readResource(requestedResourceURL), lastModified, isGzipped);
    }

    protected URL getResourceUrl(String absoluteRequestedResourcePath)
    {
        return Resources.getResource(absoluteRequestedResourcePath);
    }

    protected Optional<URL> getGzippedUrlIfExists(String requestPath)
    {
        String gzippedPath = requestPath + ".gz";
        try
        {
            return of( Resources.getResource(gzippedPath) );
        }
        catch (Exception e)
        {
            //Likely 404 error / resource not found
            return empty();
        }
    }

    protected byte[] readResource(URL requestedResourceURL) throws IOException
    {
        return Resources.toByteArray(requestedResourceURL);
    }

    private boolean isCachedClientSide(HttpServletRequest req, CachedAsset cachedAsset)
    {
        return cachedAsset.getETag().equals(req.getHeader(HttpHeaders.IF_NONE_MATCH)) ||
               (req.getDateHeader(HttpHeaders.IF_MODIFIED_SINCE) >= cachedAsset.getLastModifiedTime());
    }

    protected void setNoCacheHeaders(HttpServletResponse resp)
    {
        long now = Instant.now().toEpochMilli();

        resp.setDateHeader(HttpHeaders.DATE, now );
        resp.setDateHeader( HttpHeaders.LAST_MODIFIED, now );
        resp.setDateHeader(HttpHeaders.EXPIRES, 0);
        resp.setHeader(HttpHeaders.PRAGMA, "no-cache" );
        resp.setHeader( HttpHeaders.CACHE_CONTROL, "no-cache, must-revalidate, pre-check=0, post-check=0" );
    }

    protected void setCacheHeaders(HttpServletResponse resp)
    {
        resp.setDateHeader( HttpHeaders.EXPIRES, cacheExpireTime() );
        resp.setHeader( HttpHeaders.CACHE_CONTROL, "max-age=" + YEAR_IN_MINUTES + ", public" );
        resp.setHeader( HttpHeaders.PRAGMA, "" );
    }

    /**
     * Parses a given Range header for one or more byte ranges.
     *
     * @param rangeHeader Range header to parse
     * @param resourceLength Length of the resource in bytes
     * @return List of parsed ranges
     */
    private ImmutableList<ByteRange> parseRangeHeader(final String rangeHeader, final int resourceLength)
    {
        final ImmutableList.Builder<ByteRange> builder = ImmutableList.builder();
        if (rangeHeader.indexOf("=") != -1) {
            final String[] parts = rangeHeader.split("=");
            if (parts.length > 1) {
                final List<String> ranges = Splitter.on(",").trimResults().splitToList(parts[1]);
                for (final String range : ranges) {
                    builder.add(ByteRange.parse(range, resourceLength));
                }
            }
        }
        return builder.build();
    }

    static long cacheExpireTime()
    {
        return Instant.now().plus(Duration.ofDays(365)).toEpochMilli();
    }
}
