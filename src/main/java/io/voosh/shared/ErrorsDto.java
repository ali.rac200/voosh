package io.voosh.shared;

import java.util.List;

public interface ErrorsDto
{
    void setErrors(List<String> errors);
    List<String> getErrors();
}
