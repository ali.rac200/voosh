package io.voosh.shared;

import java.util.Collections;
import java.util.Map;
import java.util.logging.Logger;

public class Progress implements Dtos.Progress
{
    private final Logger log = Logging.get(this);

    private Status status;
    private String message;
    private double progress;
    private Map<String, String> results = Collections.emptyMap();

    public Progress()
    {
    }

    public Progress(Status status,
                    String message,
                    double progress)
    {
        this.status = status;
        this.message = message;
        this.progress = progress;
    }

    @Override
    public Status getStatus()
    {
        return status;
    }

    @Override
    public String getMessage()
    {
        return message;
    }

    @Override
    public double getProgress()
    {
        return progress;
    }

    @Override
    public Map<String, String> getResults()
    {
        return results;
    }

    @Override
    public void setStatus(Status status)
    {
        this.status = status;
    }

    @Override
    public void setMessage(String message)
    {
        this.message = message;
    }

    @Override
    public void setProgress(double progress)
    {
        this.progress = progress;
    }

    @Override
    public void setResults(Map<String, String> results)
    {
        this.results = results;
    }
}
