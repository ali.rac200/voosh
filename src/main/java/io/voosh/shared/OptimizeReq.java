package io.voosh.shared;


public class OptimizeReq implements Dtos.OptimizeReq
{
    private String url;

    @Override
    public String getUrl()
    {
        return url;
    }

    @Override
    public void setUrl(String url)
    {
        this.url = url;
    }

    @Override
    public String toString()
    {
        return "OptimizeReq{" +
               "url='" + url + '\'' +
               '}';
    }
}
