package io.voosh.shared;

import java.util.Map;

public class Dtos
{
    public interface OptimizeReq
    {
        String getUrl();
        void setUrl(String url);
    }


    public interface Progress
    {
        Status getStatus();
        String getMessage();
        double getProgress();
        Map<String, String> getResults();

        void setStatus(Status status);
        void setMessage(String message);
        void setProgress(double progress);
        void setResults(Map<String, String> results);
    }
}
