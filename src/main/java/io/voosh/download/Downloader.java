package io.voosh.download;

import io.voosh.api.Project;
import io.voosh.api.Resource;
import io.voosh.config.VooshConfiguration;
import io.voosh.shared.Logging;
import io.voosh.shared.Progress;
import io.voosh.shared.Status;
import io.voosh.vendor.HttpClient;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import javax.ws.rs.core.Response;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.logging.Logger;

public class Downloader implements Callable<Resource>
{
    @FunctionalInterface
    public interface Listener
    {
        void onUpdate(Progress update) throws Exception;
    }

    private final Logger log = Logging.get(this);
    private final VooshConfiguration config;
    private final HttpClient client;
    private final URL url;

    private Listener listener;

    private Project project;
    private List<Resource> resources = new ArrayList<>(10);

    private Elements linkElements;

    public Downloader(VooshConfiguration config, HttpClient client, String url) throws MalformedURLException
    {
        this.config = config;
        this.client = client;
        this.url = new URL(url);
    }

    public Downloader setListener(Listener listener)
    {
        this.listener = listener;
        return this;
    }

    @Override
    public Resource call() throws Exception
    {
        Response resp = client.get(url.toExternalForm());
        Resource resource = Resource.fromResponse(url.toExternalForm(), resp);


        project = new Project(url.toExternalForm());

        config.getDb().projects().save(project);
        log.info("Saved proj: " + project);

        //resource.setProjectId( project.getId() );

        //config.getDb().resources().save(resource);

        resources.add(resource);


        Map<String, String> links = parseLinks(resource);

        if (links.isEmpty())
        {
            //fixme DRY
            Progress finalProg = new Progress(Status.FINISHED, "Finished!", 100);
            finalProg.setResults( Collections.emptyMap());
            listener.onUpdate(new Progress(Status.FINISHED, "Finished!", 100));
        }

        int i = 0;
        Map<String, String> finalResults = new HashMap<>(links.size() + 1);
        finalResults.put( resource.getUrl(), "/page/" + resource.getId() );

        for (String url : links.keySet())
        {

            i++;
            double progress = getProgress(i, links.size());
            log.warning("prog : " + progress);
            String msg = String.format("%s (%d / %d)", links.get(url), i, links.size());

            listener.onUpdate(new Progress(Status.PROGRESS, msg, progress));

            Response resp2 = client.get(url);
            Resource res2 = Resource.fromResponse(url, resp2);

            res2.setProjectId(project.getId());

            try
            {
                //config.getDb().resources().save(res2);

                finalResults.put( "/page/" + res2.getId(), res2.getTitle() );
            }
            catch (Exception ignored) {}
        }

        Progress finalProg = new Progress(Status.FINISHED, "Finished!", 100 );
        finalProg.setResults( links );
        log.info("results: " + finalResults);
        listener.onUpdate(finalProg);

        return resource;
    }

    private Map<String, String> parseLinks(Resource base) throws MalformedURLException
    {
        log.info("getting doc, url: " + base.getUrl());
        Document doc = Jsoup.parse(base.getContent());
        Elements links = doc.select("a");

        linkElements = links;

        log.info("Parsed links: " + links.size() + " , total els: " + doc.children().size());

        Map<String, String> linksMap = new HashMap<>( 10 ); //Fixme put max links in config

        for (Element link : links)
        {
            try
            {
                String url = link.absUrl("href");
                if (url == null || url.trim().isEmpty())
                    continue;

                if ( ! isUrlInTargetDomain(url) )
                {
                    log.info("Ignoring url : " + url);
                    continue;
                }

                String title = link.ownText();
                if (title == null || title.trim().isEmpty())
                    title = url;

                linksMap.put(url, title);

                if (linksMap.size() >= 10) //fixme put it in config
                    return linksMap;
            }
            catch (MalformedURLException e)
            {
                log.info("Failed to parse url: " + e);
            }
        }

        return linksMap;
    }

    private boolean isUrlInTargetDomain(String url) throws MalformedURLException
    {
        return new URL(url).getAuthority().toLowerCase().contains( this.url.getHost().toLowerCase() );
    }

    private double getProgress(int current, int total)
    {
        return (Double.valueOf(current) / Double.valueOf(total)) * 100D;
    }
}
