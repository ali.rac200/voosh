package io.voosh.db;

import java.util.List;
import java.util.Optional;

public interface DbCollection<T>
{
    void save(T object);
    void saveOrOverWrite(T object);
    List<T> scan();
    Optional<T> first();
    Optional<T> findBy(String partitionKey,
                       Object val,
                       Optional<String> indexName);
    Optional<T> findBy(String partitionKey,
                       String partitionKeyVal,
                       String sortKey,
                       String sortKeyVal);


    Optional<T> find(String id);

    void delete(String id);
    void delete(T item);
}
