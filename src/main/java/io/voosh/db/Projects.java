package io.voosh.db;

import io.voosh.api.Project;
import io.voosh.shared.Logging;

import java.util.logging.Logger;

public class Projects extends DynamoDbCollection<Project>
{

    private final Logger log = Logging.get(this);

    public Projects(Db db)
    {
        super(Project.class, db);
    }
}
