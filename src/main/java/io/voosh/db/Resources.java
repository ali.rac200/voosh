package io.voosh.db;

import io.voosh.api.Resource;
import io.voosh.shared.Logging;

import java.util.logging.Logger;

public class Resources extends DynamoDbCollection<Resource>
{

    private final Logger log = Logging.get(this);

    public Resources(Db db)
    {
        super(Resource.class, db);
    }
}
