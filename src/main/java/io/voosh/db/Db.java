package io.voosh.db;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import io.voosh.config.VooshConfiguration;

public class Db
{
    private final VooshConfiguration config;
    private final AmazonDynamoDB client;
    private final DynamoDBMapper mapper;

    private final Projects projects;
    private final Resources resources;

    public Db(VooshConfiguration config, AmazonDynamoDB client, DynamoDBMapper mapper)
    {
        this.client = client;
        this.config = config;
        this.mapper = mapper;

        projects = new Projects(this);
        resources = new Resources(this);
    }

    //Exposing client since its used in a health check to ensure the db has been setup correctly.
    public AmazonDynamoDB client()
    {
        return client;
    } //Exposed to be used in test

    DynamoDBMapper mapper()
    {
        return mapper;
    }

    VooshConfiguration config()
    {
        return config;
    }

    public Projects projects()
    {
        return projects;
    }

    public Resources resources()
    {
        return resources;
    }
}
