package io.voosh.resources;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.voosh.config.VooshConfiguration;
import io.voosh.download.Downloader;
import io.voosh.shared.Logging;
import io.voosh.shared.OptimizeReq;
import io.voosh.shared.Progress;
import io.voosh.shared.Status;
import io.voosh.vendor.HttpClient;
import org.apache.commons.validator.routines.UrlValidator;

import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.logging.Logger;

@ServerEndpoint("/process")
public class ProcessorResource
{
    private final Logger log = Logging.get(this);

    //Fuck this. P.S make sure this class is instantiated AFTER this var has been set.
    private final VooshConfiguration config = VooshConfiguration.INSTANCE;

    private final ObjectMapper mapper = new ObjectMapper();
    private final UrlValidator urlValidator = new UrlValidator(new String[]{"http","https"});
    private final HttpClient client = new HttpClient( config );
    @OnOpen
    public void onOpen(Session session) throws IOException
    {
        log.info("Opened");
    }

    @OnMessage
    public void onMessage(Session session, String msg) throws Exception
    {
        //Fixme properly handle bad json
        if (msg == null || msg.trim().isEmpty() || ! msg.contains("url"))
            return;

        OptimizeReq req = mapper.readValue(msg, OptimizeReq.class);

        if (req.getUrl() == null || req.getUrl().trim().isEmpty() || ! urlValidator.isValid(req.getUrl()))
        {
            sendUpdate(new Progress(Status.ERROR, "Please provide a valid URL", 0), session);
            return;
        }

        sendUpdate(new Progress(Status.PROGRESS, "Downloading " + req.getUrl(), 0), session);
        try
        {
            new Downloader(config, client, req.getUrl()).setListener(progress -> sendUpdate(progress, session)).call();
        }
        catch (Exception e) {e.printStackTrace();}
    }

    private void sendUpdate(Progress update, Session session) throws IOException
    {
        update.setProgress( Math.round( update.getProgress() )  );
        String json = mapper.writeValueAsString( update );
        session.getBasicRemote().sendText(json);
    }
}
