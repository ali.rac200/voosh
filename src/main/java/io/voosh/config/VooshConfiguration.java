package io.voosh.config;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.Configuration;
import io.voosh.core.Mode;
import io.voosh.db.Db;
import io.voosh.vendor.HttpClient;

public class VooshConfiguration extends Configuration
{
    //THIS SHIT CANNOT BE AVOIDED, FUCK THIS
    public static VooshConfiguration INSTANCE;



    @JsonProperty
    private Mode mode;

    @JsonProperty("aws")
    private AwsConfig awsConfig = new AwsConfig();


    @JsonIgnore
    private HttpClient client;

    @JsonIgnore
    private Db db;

    public HttpClient getClient()
    {
        return client;
    }

    public void setClient(HttpClient client)
    {
        this.client = client;
    }

    public Mode getMode()
    {
        return mode;
    }

    public AwsConfig getAwsConfig()
    {
        return awsConfig;
    }

    public void setAwsConfig(AwsConfig awsConfig)
    {
        this.awsConfig = awsConfig;
    }

    public void setMode(Mode mode)
    {
        this.mode = mode;
    }

    public Db getDb()
    {
        return db;
    }

    public void setDb(Db db)
    {
        this.db = db;
    }
}