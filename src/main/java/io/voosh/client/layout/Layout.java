package io.voosh.client.layout;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.RootPanel;
import io.voosh.client.route.Route;
import io.voosh.client.view.View;
import io.voosh.shared.Logging;

import java.util.logging.Logger;

public class Layout extends Composite
{
    interface ViewUiBinder extends UiBinder<Panel, Layout> {};
    private static final ViewUiBinder binder =  GWT.create(ViewUiBinder.class);

    private static Layout instance;
    private final Logger log = Logging.get(this);

    @UiField Panel root;

    private View lastView;
    public static void init()
    {
        instance = new Layout();
    }

    public static Layout get()
    {
        return instance;
    }


    @SuppressWarnings("EqualsBetweenInconvertibleTypes")
    private Layout()
    {
        initWidget( binder.createAndBindUi(this) );

        RootPanel.get("root").clear(true);
        RootPanel.get("root").add(this);
    }

    public Layout showLoading(boolean show)
    {
        return this;
    }

    public void setRootStyles(String styles)
    {
        root.setStyleName(styles);
    }


    public Layout createAndDestroy(Route route, View newView)
    {
        showLoading(false);
        if (lastView != null)
        {
            lastView.destroy();
            root.remove(lastView);
            //layout.setHeading( route.title() );
        }

        root.add(newView);
        newView.create();

        lastView = newView;
        return this;
    }

}