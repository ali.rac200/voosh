package io.voosh.client.route;

public interface Routable
{
    public void route(RouteResult route);
}
