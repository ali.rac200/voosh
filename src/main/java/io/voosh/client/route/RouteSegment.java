package io.voosh.client.route;

public class RouteSegment
{
    private int index;
    private final String name;
    private final boolean isParam;
    private final boolean isOptional;

    private RouteParam<?> param;

    public RouteSegment(int index, String name)
    {
        this.index = index;
        this.name = getActualName(name);
        this.isParam = isParam(name);
        this.isOptional = isOptional(name);
    }

    public String name()
    {
        return name;
    }

    public boolean isParam()
    {
        return isParam;
    }

    public boolean isOptional()
    {
        return isOptional;
    }

    public void setParam(RouteParam<?> param)
    {
        this.param = param;
    }

    public RouteParam<?> getParam()
    {
        return param;
    }

    public int index()
    {
        return index;
    }

    public static String getActualName(String path)
    {
        if (path.isEmpty() )
            return path;
        int start = 0;
        for (int i = 0; i <= 1; i++)
        {
            if ( path.length() <= i )
                return path;
            char toCheck = path.charAt(i);
            if (toCheck == '?' || toCheck == ':')
                start = i+1;
        }

        return path.substring(start);
    }

    public static boolean isParam(String path)
    {
        return path.startsWith(":");
    }

    public static boolean isOptional(String path)
    {
        if (path.startsWith("?"))
            return true;
        if (path.length() <= 1)
            return false;

        return path.charAt(1) == '?';
    }
}