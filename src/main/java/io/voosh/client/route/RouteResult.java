package io.voosh.client.route;


import com.google.gwt.http.client.URL;
import io.voosh.shared.Logging;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

public class RouteResult
{
    private final Logger log = Logging.get(this);
    private final Route route;
    private final List<String> segments = new ArrayList<>();
    private final Map<String, RouteParam<?>> params = new HashMap<>();
    private String action;

    public RouteResult(Route route)
    {
        this.route = route;
    }

    public void addSegment(String segment)
    {
        segments.add(segment);
    }

    public void addParam(RouteParam<?> param)
    {
        this.params.put(param.name(), param);
    }

    public List<RouteParam<?>> getParams()
    {
        ArrayList<RouteParam<?> > list = new ArrayList<>();
        for (RouteParam<?> param : params.values() )
        {
            list.add( param );
        }

        return list;
    }

    @SuppressWarnings(value="unchecked")
    public <T> RouteParam<T> getParam(String name)
    {
        if (! params.containsKey(name))
            return null;
        return (RouteParam<T>) params.get(name);
    }

    @SuppressWarnings(value="unchecked")
    public <T> T paramVal(String name)
    {
        if (! params.containsKey(name))
            return null;

        RouteParam<T> param = (RouteParam<T>) params.get(name);
        return param.value();
    }

    public String strParam(String name)
    {
        return paramVal(name);
    }


    /**
     * Checks if the given parameter was provided with the url. If so,
     * decodes it via URL.decodePathSegment and returns it. If not, returns
     * an empty string.
     *
     * decodePathSegment is a better choice since individual params are going
     * to be path segments. For decoding query strings which are params,
     * use RouteResult#decodeQueryString (//TODO)
     */
    public String decodedParam(String name)
    {
        String param = param(name);

        if (param.isEmpty() )
        {
            log.warning("Empty param: " + name);
            return param;
        }

        return URL.decodePathSegment(param).trim();
    }

    public String param(String name)
    {
        return (has(name)) ? strParam(name).trim() : "";
    }

    public Long getId()
    {
        return this.<Long> getParam("id").value();
    }

    /**
     * Returns the command of the given route. E.g in calendar/add , the command is
     * add, while in calendar/edit/xxx , the command is edit.
     */
    public String cmd()
    {
        if ( segments.size() >= 2 )
            return segments.get(1);
        else
            return segments.get(0);
    }

    public Route route()
    {
        return route;
    }

    @Override
    public String toString()
    {
        final StringBuilder sb = new StringBuilder("RouteResult{");
        sb.append("route=").append(route);
        sb.append(", segments=").append(segments);
        sb.append(", params=").append(params);
        sb.append(", action='").append(action).append('\'');
        sb.append('}');
        return sb.toString();
    }

    public boolean has(String paramName)
    {
        return params.containsKey(paramName) && params.get(paramName) != null;
    }
}
