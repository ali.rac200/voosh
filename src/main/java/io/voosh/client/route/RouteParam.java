package io.voosh.client.route;


public final class RouteParam<T>
{
    private final String name;
    private T value;
    private final boolean hasRequest;

    public RouteParam(String name)
    {
        this.name = name;
        hasRequest = false;
    }

    public String name()
    {
        return name;
    }

    public boolean hasRequest()
    {
        return hasRequest;
    }

    public boolean validate(String value)
    {
        return true;
    }

    public T value()
    {
        return value;
    }

    @Override
    public String toString()
    {
        final StringBuilder sb = new StringBuilder("RouteParam{");
        sb.append("name='").append(name).append('\'');
        sb.append(", value=").append(value);
        sb.append('}');
        return sb.toString();
    }
}
