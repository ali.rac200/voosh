package io.voosh.client.service;

import io.voosh.shared.ErrorsDto;
import io.voosh.shared.Logging;
import io.voosh.shared.util.BeanUtil;
import io.voosh.shared.util.Callback;
import org.fusesource.restygwt.client.FailedResponseException;
import org.fusesource.restygwt.client.Method;
import org.fusesource.restygwt.client.MethodCallback;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class Service<I, O>
{
    private final Logger log = Logging.get(this);
    private final Callback<O> successHandler;

    private final Callback<ErrorsDto> errorHandler;

    private final ApiService<I, O> request;

    public Service(ApiService<I, O> request,
                   Callback<O> successHandler,
                   Callback<ErrorsDto> errorHandler)
    {
        this.successHandler = successHandler;
        this.errorHandler = errorHandler;
        this.request = request;
    }

    public void make(I input)
    {
        log.info("Making req: " + input);

        request.makeReq(input, new MethodCallback<O>()
        {
            @Override
            public void onFailure(Method method, Throwable throwable)
            {
                if (throwable instanceof FailedResponseException)
                {
                    FailedResponseException e = (FailedResponseException) throwable;
                    log.severe("Req failed: " + e.getResponse().getText());
                    ErrorsDto errors =
                            BeanUtil.decode(e.getResponse().getText(), ErrorsDto.class);
                    errorHandler.accept(errors);
                }
                else
                {
                    ErrorsDto errors = BeanUtil.FACTORY.errors().as();

                    List<String> msgs = new ArrayList<>(1);
                    msgs.add(throwable.getMessage() );
                    errors.setErrors(msgs);

                    errorHandler.accept(errors);
                }
            }

            @Override
            public void onSuccess(Method method, O o)
            {
                successHandler.accept(o);
            }
        });
    }
}
