package io.voosh.client.ui;

import com.google.gwt.dom.client.Element;
import com.google.gwt.user.client.ui.HTMLPanel;

public class Form extends HTMLPanel
{
    private Runnable onSubmit;

    public Form(String html)
    {
        super("form", html);
        setEvent( getElement() );
    }

    public Form setOnSubmit(Runnable onSubmit)
    {
        this.onSubmit = onSubmit;
        return this;
    }

    private void handleOnSubmit()
    {
        if (onSubmit != null)
            onSubmit.run();
    }

    private native void setEvent(Element form)
    /*-{
            var that = this;
            form.onsubmit = function()
            {
                 try
                 {
                     $entry(function()
                        {
                            that.@io.voosh.client.ui.Form::handleOnSubmit()();
                        })();
                 }
                catch(e) { $wnd.console.error(e)};
                return false;
            };
    }-*/;
}
