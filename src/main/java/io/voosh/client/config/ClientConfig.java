package io.voosh.client.config;

public interface ClientConfig
{
    //No trailing slash at end
    String apiUrl();

    String wsUrl(); //Same as above
}
