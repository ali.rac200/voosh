package io.voosh.client.config;

import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.NodeList;
import io.voosh.shared.Logging;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

public class MetaTagClientConfig implements ClientConfig
{
    private static final String HOME_URL_KEY = "apiUrl";
    private static final String WS_KEY = "wsUrl";

    private final Logger log = Logging.get(this);

    private final String apiUrl;

    private final String wsUrl;

    public MetaTagClientConfig()
    {
        NodeList<Element> tags = Document.get().getElementsByTagName("meta");
        Map<String, String> tagContents = new HashMap<>( tags.getLength() );
        for (int i = 0; i < tags.getLength(); i++)
        {
            Element tag = tags.getItem(i);
            tagContents.put(tag.getAttribute("name"), tag.getAttribute("content"));
        }
        apiUrl = parseHomeUrl( tagContents.get(  HOME_URL_KEY ) );
        wsUrl = tagContents.get( WS_KEY );

        log.info("Websocket url: " + wsUrl);
    }

    private String parseHomeUrl(String url)
    {
        if (url == null)
            throw new IllegalStateException( HOME_URL_KEY + " not found in Configuration");

        //Ensure no trailing slash
        if (url.endsWith("/"))
            url = url.substring(0, url.length() - 1);

        return url;
    }
    @Override
    public String apiUrl()
    {
        return apiUrl;
    }

    @Override
    public String wsUrl()
    {
        return wsUrl;
    }


}
