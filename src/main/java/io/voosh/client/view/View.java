package io.voosh.client.view;

import com.google.gwt.user.client.ui.IsWidget;

public interface View extends IsWidget
{
    void create();
    void destroy();

    String pageStyles();
}
