package io.voosh.client.view;

import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.Widget;
import io.voosh.client.res.Res;

public abstract class BaseView implements View
{
    @UiField public Res res;
    protected Panel root;

    protected void init( Panel root )
    {
        this.root = root;
    }

    @Override
    public Widget asWidget()
    {
        return root;
    }

    @Override
    public void create()
    {

    }

    @Override
    public void destroy()
    {

    }
}
