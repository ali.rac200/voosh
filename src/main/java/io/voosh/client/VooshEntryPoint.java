package io.voosh.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import io.voosh.client.config.ClientConfig;
import io.voosh.client.config.MetaTagClientConfig;
import io.voosh.client.layout.Layout;
import io.voosh.client.nav.FrontNavigator;
import io.voosh.client.nav.Navigator;
import io.voosh.client.ui.Ui;
import io.voosh.shared.Logging;
import io.voosh.shared.util.BeanUtil;
import io.voosh.shared.util.Beans;

import java.util.logging.Logger;

public class VooshEntryPoint implements EntryPoint
{
   private static VooshEntryPoint INSTANCE;

    private final Logger log = Logging.get(this);

    private ClientConfig config;

    public static VooshEntryPoint get()
    {
        return INSTANCE;
    }

    public static void handleException(Throwable e)
    {
        new SdmExceptionHandler().onUncaughtException(e);
    }

    public static String appName()
    {
        return "Voosh";
    }

    @Override
    public void onModuleLoad()
    {
        GWT.setUncaughtExceptionHandler(VooshEntryPoint::handleException);
        init(new MetaTagClientConfig());
    }

    public ClientConfig config()
    {
        return config;
    }


    @SuppressWarnings("ConstantConditions")
    public void init(ClientConfig config)
    {
        INSTANCE = this;
        BeanUtil.setFactory( GWT.create(Beans.class));

        //Defaults.setDateFormat(null);
        //Defaults.setServiceRoot( config.apiUrl() );

        this.config = config;

        uiInit();
    }


    private void uiInit()
    {
        Layout.init();
        Navigator.init();
        Ui.init();
        FrontNavigator.init();

        Navigator.get().navigate();
    }

    public static void prompt(String msg)
    {
        Window.alert(msg);
    }
}
