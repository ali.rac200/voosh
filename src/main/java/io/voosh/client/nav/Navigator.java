package io.voosh.client.nav;

import com.google.gwt.dom.client.Document;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.History;
import io.voosh.client.VooshEntryPoint;
import io.voosh.client.layout.Layout;
import io.voosh.client.route.Route;
import io.voosh.client.route.RouteResolver;
import io.voosh.client.route.RouteResult;
import io.voosh.client.view.View;
import io.voosh.shared.Logging;
import io.voosh.shared.util.Callback;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

public class Navigator implements ValueChangeHandler<String>
{
    private static Navigator instance;
    private final Logger log = Logging.get(this);

    private final RouteResolver rr;
    private final Map<Route, Callback<RouteResult>> handlers = new HashMap<>();

    private String lastToken = "";
    private String currentToken = "";
    private String lastWorkingToken = "";

    //Only contains a history of previously working tokens
    private final List<String> history = new ArrayList<>();
    private int routeCount = 0; //incremented with each successful route change, minus error routes.

    private boolean initialized = false;

    private RouteResult route;

    public static void init()
    {
        //Set instance as the first line, otherwise Navigator.get() would be
        //null if called from elsewhere. Constructor must be noop, all init
        //code must be put in this init() method.

        instance = new Navigator( RouteResolver.get() );
        History.addValueChangeHandler(instance);
    }

    public static Navigator get()
    {
        return instance;
    }


    private Navigator(RouteResolver rr)
    {
        this.rr = rr;
    }

    public Navigator navigate()
    {
        initialized = true;
        History.fireCurrentHistoryState();

        return this;
    }

    public Navigator addHandler(Route route, Callback<RouteResult> handler)
    {
        handlers.put(route, handler);
        return this;
    }


    public void goTo(String token)
    {
        History.newItem(token);
    }

    public void goTo(Route route)
    {
        goTo(route.token());
    }

    @Override
    public void onValueChange(ValueChangeEvent<String> event)
    {
        try
        {
            processToken(event.getValue());
        } catch (Exception e)
        {
            VooshEntryPoint.handleException(e);
        }
    }


    public String getLastToken()
    {
        return lastToken;
    }

    public Route getRoute()
    {
        return route.route();
    }

    public Navigator setLastWorkingToken(String token, Route route)
    {
        //if (token.equals(Route.ERROR.token()))
          //  return this;

        if (routeCount > 0)
            history.add(lastWorkingToken);

        this.lastWorkingToken = token;
        routeCount++;
        return this;
    }

    public String getLastWorkingToken()
    {
        return lastWorkingToken;
    }

    public String getCurrentToken()
    {
        return currentToken;
    }

    public int getHistoryCount()
    {
        return history.size();
    }

    public boolean hasHistory()
    {
        return getHistoryCount() > 0;
    }

    public void goBack(String defaultToken)
    {
        String token = (history.isEmpty())
                       ? defaultToken
                       : history.get(history.size() - 1);


        goTo(token);
    }

    public void goBack(Route defaultRoute)
    {
        goBack(defaultRoute.token());
    }

    public Navigator addToHistory(String token)
    {
        History.newItem(token);
        return this;
    }

    /**
     * Called after the presenter has been created, to route it
     * according to the RouteResult if necessary.
     */
    /*
    public Navigator route(Presenter presenter)
    {
        if (!(presenter instanceof Routable))
            return this;

        Console.debug("Routing " + presenter + " to " + route);
        ((Routable) presenter).route(route);
        return this;
    }*/

    public Navigator setWindowTitle(Route route)
    {
        StringBuilder sb = new StringBuilder();

        sb.append( route.title() )
            .append(" | ").append(VooshEntryPoint.appName());

        Document.get().setTitle(sb.toString());

        return this;
    }

    public Navigator setWindowTitle(String title,
                                    Route route)
    {
        String routeName = route.title() ;

        if (routeName.trim().toLowerCase().equals(title.trim().toLowerCase()))
            return this;

        StringBuilder sb = new StringBuilder();
        sb.append(title);
        if (! routeName.isEmpty() )
                sb.append(" | ").append(routeName);

        sb.append(" | ").append(VooshEntryPoint.appName());

        Document.get().setTitle(sb.toString());
        return this;
    }

    public void setWindowTitle(String title)
    {
        StringBuilder sb = new StringBuilder();
        sb.append(title);
        sb.append(" |" ).append(VooshEntryPoint.appName());

        Document.get().setTitle(sb.toString());
    }

    private void processToken(String token)
    {
        if (!initialized)
            return;
        token = token.trim();

        route = rr.resolve(token);

        log.info("Route resolved: " + route);
        if (route == null)
        {
            //VooshEntryPoint.handleException(new RuntimeException("Page not found: " + token));
            log.warning("Page not found: " + token + ", defaulting to default token");
            goTo("");
            return;
        }

        this.lastToken = this.currentToken;
        this.currentToken = token;

        Route route =   this.route.route();

       if (!route.isAvailable())
        {
            VooshEntryPoint.handleException(new RuntimeException("Unauthorized"));
            return;
        }

        for (Map.Entry<Route, Callback<RouteResult>> handler : handlers.entrySet() )
        {
            if (handler.getKey() != route )
                continue;

            handler.getValue().accept(this.route);
            return;
        }
        throw new IllegalStateException("No route handler found for : " + route);
    }



    void end(Route route, View currentView)
    {
        setLastWorkingToken(currentToken, route);
        setWindowTitle(route);
        Layout.get().createAndDestroy(route, currentView);
    }
}
